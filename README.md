# HIPO CSS


## _ => reverse 
## h => horizontal
## -h => horizontal stretch
## v => vertical
## -v => vertical stretch

## t => centrado arriba
## tl => izquierda arriba
## tr => derecha arriba
## b => centrado abajo
## bl => izquierda abajo
## br => derecha abajo
## l => centrado izquierda
## r => centrado derecha

## s => scroll

## a => space-around
## o => space-between
## e => space-evenly